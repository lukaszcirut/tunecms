<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Metas
 *
 * @ORM\Table(name="metas", uniqueConstraints={@ORM\UniqueConstraint(name="metas_slug_unique", columns={"slug"})})
 * @ORM\Entity
 */
class Metas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", length=65535, nullable=false)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="short_text", type="text", length=65535, nullable=false)
     */
    private $shortText;

    /**
     * @var string
     *
     * @ORM\Column(name="additional1", type="text", length=65535, nullable=false)
     */
    private $additional1;

    /**
     * @var string
     *
     * @ORM\Column(name="additional2", type="text", length=65535, nullable=false)
     */
    private $additional2;

    /**
     * @var string
     *
     * @ORM\Column(name="additional3", type="text", length=65535, nullable=false)
     */
    private $additional3;

    /**
     * @var int
     *
     * @ORM\Column(name="language_id", type="integer", nullable=false)
     */
    private $languageId;

    /**
     * @var int
     *
     * @ORM\Column(name="entity_id", type="integer", nullable=false)
     */
    private $entityId;

    /**
     * @var int
     *
     * @ORM\Column(name="entity_type_id", type="integer", nullable=false)
     */
    private $entityTypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     */
    private $slug;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt = '0000-00-00 00:00:00';



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Metas
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set shortText
     *
     * @param string $shortText
     *
     * @return Metas
     */
    public function setShortText($shortText)
    {
        $this->shortText = $shortText;

        return $this;
    }

    /**
     * Get shortText
     *
     * @return string
     */
    public function getShortText()
    {
        return $this->shortText;
    }

    /**
     * Set additional1
     *
     * @param string $additional1
     *
     * @return Metas
     */
    public function setAdditional1($additional1)
    {
        $this->additional1 = $additional1;

        return $this;
    }

    /**
     * Get additional1
     *
     * @return string
     */
    public function getAdditional1()
    {
        return $this->additional1;
    }

    /**
     * Set additional2
     *
     * @param string $additional2
     *
     * @return Metas
     */
    public function setAdditional2($additional2)
    {
        $this->additional2 = $additional2;

        return $this;
    }

    /**
     * Get additional2
     *
     * @return string
     */
    public function getAdditional2()
    {
        return $this->additional2;
    }

    /**
     * Set additional3
     *
     * @param string $additional3
     *
     * @return Metas
     */
    public function setAdditional3($additional3)
    {
        $this->additional3 = $additional3;

        return $this;
    }

    /**
     * Get additional3
     *
     * @return string
     */
    public function getAdditional3()
    {
        return $this->additional3;
    }

    /**
     * Set languageId
     *
     * @param int $languageId
     *
     * @return Metas
     */
    public function setLanguageId($languageId)
    {
        $this->languageId = $languageId;

        return $this;
    }

    /**
     * Get languageId
     *
     * @return int
     */
    public function getLanguageId()
    {
        return $this->languageId;
    }

    /**
     * Set entityId
     *
     * @param int $entityId
     *
     * @return Metas
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * Get entityId
     *
     * @return int
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * Set entityTypeId
     *
     * @param int $entityTypeId
     *
     * @return Metas
     */
    public function setEntityTypeId($entityTypeId)
    {
        $this->entityTypeId = $entityTypeId;

        return $this;
    }

    /**
     * Get entityTypeId
     *
     * @return int
     */
    public function getEntityTypeId()
    {
        return $this->entityTypeId;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Metas
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Metas
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Metas
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
