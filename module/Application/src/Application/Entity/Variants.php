<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Variants
 *
 * @ORM\Table(name="variants")
 * @ORM\Entity
 */
class Variants
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="product_id", type="integer", nullable=false)
     */
    private $productId;

    /**
     * @var int
     *
     * @ORM\Column(name="currency_id", type="integer", nullable=false)
     */
    private $currencyId;

    /**
     * @var int
     *
     * @ORM\Column(name="status_id", type="integer", nullable=false)
     */
    private $statusId;

    /**
     * @var float
     *
     * @ORM\Column(name="netto", type="float", precision=8, scale=4, nullable=false)
     */
    private $netto;

    /**
     * @var float
     *
     * @ORM\Column(name="brutto", type="float", precision=8, scale=4, nullable=false)
     */
    private $brutto;

    /**
     * @var float
     *
     * @ORM\Column(name="buy_netto", type="float", precision=8, scale=4, nullable=false)
     */
    private $buyNetto;

    /**
     * @var float
     *
     * @ORM\Column(name="buy_brutto", type="float", precision=8, scale=4, nullable=false)
     */
    private $buyBrutto;

    /**
     * @var int
     *
     * @ORM\Column(name="vat", type="integer", nullable=false)
     */
    private $vat;

    /**
     * @var int
     *
     * @ORM\Column(name="margin", type="integer", nullable=false)
     */
    private $margin;

    /**
     * @var string
     *
     * @ORM\Column(name="features", type="text", length=65535, nullable=false)
     */
    private $features;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt = '0000-00-00 00:00:00';



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Variants
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set productId
     *
     * @param int $productId
     *
     * @return Variants
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set currencyId
     *
     * @param int $currencyId
     *
     * @return Variants
     */
    public function setCurrencyId($currencyId)
    {
        $this->currencyId = $currencyId;

        return $this;
    }

    /**
     * Get currencyId
     *
     * @return int
     */
    public function getCurrencyId()
    {
        return $this->currencyId;
    }

    /**
     * Set statusId
     *
     * @param int $statusId
     *
     * @return Variants
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return int
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set netto
     *
     * @param float $netto
     *
     * @return Variants
     */
    public function setNetto($netto)
    {
        $this->netto = $netto;

        return $this;
    }

    /**
     * Get netto
     *
     * @return float
     */
    public function getNetto()
    {
        return $this->netto;
    }

    /**
     * Set brutto
     *
     * @param float $brutto
     *
     * @return Variants
     */
    public function setBrutto($brutto)
    {
        $this->brutto = $brutto;

        return $this;
    }

    /**
     * Get brutto
     *
     * @return float
     */
    public function getBrutto()
    {
        return $this->brutto;
    }

    /**
     * Set buyNetto
     *
     * @param float $buyNetto
     *
     * @return Variants
     */
    public function setBuyNetto($buyNetto)
    {
        $this->buyNetto = $buyNetto;

        return $this;
    }

    /**
     * Get buyNetto
     *
     * @return float
     */
    public function getBuyNetto()
    {
        return $this->buyNetto;
    }

    /**
     * Set buyBrutto
     *
     * @param float $buyBrutto
     *
     * @return Variants
     */
    public function setBuyBrutto($buyBrutto)
    {
        $this->buyBrutto = $buyBrutto;

        return $this;
    }

    /**
     * Get buyBrutto
     *
     * @return float
     */
    public function getBuyBrutto()
    {
        return $this->buyBrutto;
    }

    /**
     * Set vat
     *
     * @param int $vat
     *
     * @return Variants
     */
    public function setVat($vat)
    {
        $this->vat = $vat;

        return $this;
    }

    /**
     * Get vat
     *
     * @return int
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * Set margin
     *
     * @param int $margin
     *
     * @return Variants
     */
    public function setMargin($margin)
    {
        $this->margin = $margin;

        return $this;
    }

    /**
     * Get margin
     *
     * @return int
     */
    public function getMargin()
    {
        return $this->margin;
    }

    /**
     * Set features
     *
     * @param string $features
     *
     * @return Variants
     */
    public function setFeatures($features)
    {
        $this->features = $features;

        return $this;
    }

    /**
     * Get features
     *
     * @return string
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Variants
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Variants
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
