<?php

return array(
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'Dashboard' => __DIR__ . '/../../public',
            ),
        ),
    ),
);

//return array(
//    'asset_manager' => array(
//        'resolver_configs' => array(
//            'paths' => array(
//                'Dashboard' => __DIR__ . '/../public',
//            ),
//            'collections' => array(
//                'js/d.js' => array(
//                    'js/scripts.js',
//                ),
//            ),
//            'paths' => array(
//                __DIR__ . '/../public/js',
//            ),
//            'map' => array(
////                'specific-path.css' => __DIR__ . '/../../public/file.css',
//                'scripts.js' => __DIR__ . '/../../public/file.css',
//            ),
//        ),
//        'filters' => array(
//            'js/d.js' => array(
//                array(
//                    // Note: You will need to require the classes used for the filters yourself.
//                    'filter' => 'JSMin',
//                ),
//            ),
//        ),
//        'caching' => array(
//            'js/d.js' => array(
//                'cache'     => 'Dashboard',
//            ),
//        ),
//    ),
//);