<?php

return array_merge_recursive(
    require_once __DIR__ . '/routing/routes.php',
    require_once __DIR__ . '/assets/asset_manager.php',
    array(
        'controllers' => array(
            'invokables' => array(
                'Dashboard\Controller\DashboardController' => Dashboard\Controller\DashboardController::class
            ),
        ),
        'view_manager' => array(
            'display_not_found_reason' => true,
            'display_exceptions' => true,
            'doctype' => 'HTML5',
            'not_found_template' => 'error/404',
            'exception_template' => 'error/index',
            'template_map' => array(
                'dashboard/layout' => __DIR__ . '/../view/layout/dashboard.phtml',
                'dashboard/index/index' => __DIR__ . '/../view/dashboard/dashboard/index.phtml',
                'error/404' => __DIR__ . '/../view/error/404.phtml',
                'error/index' => __DIR__ . '/../view/error/index.phtml',
            ),
            'template_path_stack' => array(
                __DIR__ . '/../view',
            ),
        ),
    )
);