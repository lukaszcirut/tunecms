$(function () {

    $("#jQ_drag").sortable({opacity: 0.6, cursor: 'move', update: function () {
            var order = $(this).sortable("serialize") + '&action=changePosition'

            $.post("/administrator/files/position/", order, function (theResponse) {
                generateNoty('info', 'Images position saved');
            });
        }
    });

    if ($('#success-message').length) {
        var json = $('#success-message').text();
        var array = JSON.parse(json);
        for (var i = 0; i < array.length; i++) {
            generateNoty('success', array[i]);
        }
    }

    $('.del-img').on('click', function () {
        var icoBtn = $(this);
        var parent = icoBtn.parents('.gallery-item');

        var id = parent.attr('data-id');

        $.ajax({
            type: "POST",
            url: '/administrator/files/delete/id/' + id,
            success: function (data) {
                if (data == 'success') {
                    parent.remove();
                    generateNoty('success', 'Image successfully removed');
                } else {
                    generateNoty('error', 'Something went wrong');
                }
            },
        });
    });

    function generateNoty(type, text) {

        var n = noty({
            text: text,
            type: type,
            dismissQueue: true,
            timeout: '200',
            layout: 'top',
            closeWith: ['click', 'button'],
            theme: 'defaultTheme',
            maxVisible: 3,
            container: 'body',
            animation: {
                open: 'animated bounceInLeft',
                close: 'animated bounceOutLeft',
                easing: 'swing',
                speed: 500
            }
        });
    }

});