<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Dashboard\Controller;

use System\Controller\AbstractDashboardController;
use User\Entity\Users;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class DashboardController extends AbstractDashboardController
{

    public function indexAction()
    {
        /** @var Users $users */
        $users = $this->getEntityManager()->getRepository('User\Entity\Users');
        $asd = $users->findAll();
//        echo '<pre>';
//        print_r($asd);
//        echo '</pre>';die;

        return new ViewModel(array(
//            'albums' => $this->getEntityManager()->getRepository('Album\Entity\Album')->findAll(),
        ));
    }
}
