<?php

namespace System\View\Box;

use System\View\Widget\AbstractWidget;

class LteBoxGenerator
{
    protected $header = '<div class="box-header  with-border">
                              <h3 class="box-title">%s</h3>
                              <div class="box-tools pull-right">
                                %s %s
                              </div>
                        </div>';
    protected $footer = '<div class="box-footer" %s>%s</div>';
    protected $content;
    protected $widget;
    protected $name;
    protected $collapseButton = '<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>';
    protected $collapsedButton = '<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>';
    protected $closeButton = '<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>';
    protected $contentHtml = '<div class="box-body" %s>%s</div>';
    protected $collapsed = false;
    protected $footerContent = '';

    protected $width = '12';


    /**
     * LteBoxGenerator constructor.
     * @param $content
     * @param $widget
     */
    public function __construct($content = null, AbstractWidget $widget = null)
    {
        $this->content = $content;
        $this->widget = $widget;
    }

    public function render()
    {
        $width = !empty($this->widget)? $this->getWidget()->getWidth() : $this->getWidth();
        $box = sprintf('<div class="col-md-%s"><div class="box box-default">', $width);
        $box .= $this->renderHeader();
        $box .= $this->renderContent();
        $box .= $this->renderFooter();
        $box .= '</div></div>';

        return $box;
    }

    private function renderHeader()
    {
        if(!$this->getHeader())
            return '';
        $title = !empty($this->widget)? $this->getWidget()->getName():$this->getName();
        if(!empty($this->getWidget()) && $this->getWidget()->isCollapseable()) {
            $collapseButton = ($this->isCollapsed()) ? $this->getCollapseButton() : $this->getCollapsedButton();
        } else {
            $collapseButton = '';
        }

        if(!empty($this->getWidget()) && $this->getWidget()->isCloseable()) {
            $closeButton = $this->getCloseButton();
        } else {
            $closeButton = '';
        }

        return sprintf($this->getHeader(), $title, $collapseButton, $closeButton);
    }

    private function renderContent()
    {
        if(!$this->content && $this->widget == null)
            return '';
        $displayStyle = ($this->isCollapsed()) ? 'style="display: none"':'style="display: block"';
        $content = !empty($this->widget)? $this->getWidget()->render():$this->getContent();
        return sprintf($this->contentHtml, $displayStyle, $content);
    }

    private function renderFooter()
    {
        if(!$this->footer)
            return '';
        $displayStyle = ($this->isCollapsed()) ? 'style="display: none"':'style="display: block"';
        return sprintf($this->footer, $displayStyle, $this->footerContent);
    }


    /**
     * @return mixed
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @param mixed $header
     */
    public function setHeader($header)
    {
        $this->header = $header;
    }

    /**
     * @return mixed
     */
    public function getFooter()
    {
        return $this->footer;
    }

    /**
     * @param mixed $footer
     */
    public function setFooter($footer)
    {
        $this->footer = $footer;
    }

    /**
     * @return null
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param null $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return AbstractWidget
     */
    public function getWidget()
    {
        return $this->widget;
    }

    /**
     * @param AbstractWidget $widget
     */
    public function setWidget($widget)
    {
        $this->widget = $widget;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getCollapseButton()
    {
        return $this->collapseButton;
    }

    /**
     * @param string $collapseButton
     */
    public function setCollapseButton($collapseButton)
    {
        $this->collapseButton = $collapseButton;
    }

    /**
     * @return string
     */
    public function getCollapsedButton()
    {
        return $this->collapsedButton;
    }

    /**
     * @param string $collapsedButton
     */
    public function setCollapsedButton($collapsedButton)
    {
        $this->collapsedButton = $collapsedButton;
    }

    /**
     * @return string
     */
    public function getCloseButton()
    {
        return $this->closeButton;
    }

    /**
     * @param string $closeButton
     */
    public function setCloseButton($closeButton)
    {
        $this->closeButton = $closeButton;
    }

    /**
     * @return boolean
     */
    public function isCollapsed()
    {
        return $this->collapsed;
    }

    /**
     * @param boolean $collapsed
     */
    public function setCollapsed($collapsed)
    {
        $this->collapsed = $collapsed;
    }

    /**
     * @return string
     */
    public function getContentHtml()
    {
        return $this->contentHtml;
    }

    /**
     * @param string $contentHtml
     */
    public function setContentHtml($contentHtml)
    {
        $this->contentHtml = $contentHtml;
    }

    /**
     * @return string
     */
    public function getFooterContent()
    {
        return $this->footerContent;
    }

    /**
     * @param string $footerContent
     */
    public function setFooterContent($footerContent)
    {
        $this->footerContent = $footerContent;
    }

    /**
     * @return string
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param string $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

}