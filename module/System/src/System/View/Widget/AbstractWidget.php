<?php

namespace System\View\Widget;

use Zend\Di\ServiceLocator;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

abstract class AbstractWidget implements ServiceLocatorAwareInterface
{
    protected $width = '6';
    protected $resizable = false;
    protected $movable = false;
    protected $closeable = true;
    protected $collapseable = true;
    protected $ajaxable = true;
    protected $lteBox = false;
    protected $mainColor = 'green';
    protected $name;
    /** @var  ServiceLocator */
    protected $serviceLocator;

    public function __construct($options = array(), $serviceLocator)
    {
        if(!empty($options))
            $this->exchangeArray($options);

        $this->serviceLocator = $serviceLocator;
    }

    private function exchangeArray(array $options)
    {
        foreach($options as $key => $value) {
            $function = 'set'.ucfirst($key);
            if(is_callable($function))
                $this->$function($value);
        }
    }

    abstract function render();

    /**
     * @return string
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param string $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return boolean
     */
    public function isResizable()
    {
        return $this->resizable;
    }

    /**
     * @param boolean $resizable
     */
    public function setResizable($resizable)
    {
        $this->resizable = $resizable;
    }

    /**
     * @return boolean
     */
    public function isMovable()
    {
        return $this->movable;
    }

    /**
     * @param boolean $movable
     */
    public function setMovable($movable)
    {
        $this->movable = $movable;
    }

    /**
     * @return boolean
     */
    public function isCloseable()
    {
        return $this->closeable;
    }

    /**
     * @param boolean $closeable
     */
    public function setCloseable($closeable)
    {
        $this->closeable = $closeable;
    }

    /**
     * @return boolean
     */
    public function isCollapseable()
    {
        return $this->collapseable;
    }

    /**
     * @param boolean $collapseable
     */
    public function setCollapseable($collapseable)
    {
        $this->collapseable = $collapseable;
    }

    /**
     * @return boolean
     */
    public function isAjaxable()
    {
        return $this->ajaxable;
    }

    /**
     * @param boolean $ajaxable
     */
    public function setAjaxable($ajaxable)
    {
        $this->ajaxable = $ajaxable;
    }

    /**
     * @return boolean
     */
    public function isLteBox()
    {
        return $this->lteBox;
    }

    /**
     * @param boolean $lteBox
     */
    public function setLteBox($lteBox)
    {
        $this->lteBox = $lteBox;
    }

    /**
     * @return string
     */
    public function getMainColor()
    {
        return $this->mainColor;
    }

    /**
     * @param string $mainColor
     */
    public function setMainColor($mainColor)
    {
        $this->mainColor = $mainColor;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return ServiceLocator
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

}