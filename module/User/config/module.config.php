<?php

return array_merge_recursive(
    require_once __DIR__ . '/routing/routes.php',
    require_once __DIR__ . '/assets/asset_manager.php',
    array(
        'controllers' => array(
            'invokables' => array(
                'User\Controller\UserController' => User\Controller\UserController::class
            ),
        ),
        'view_manager' => array(
            'template_map' => array(
                'user/user/index' => __DIR__ . '/../view/user/index.phtml',

                'user/widget/list' => __DIR__ . '/../view/user/widget/list.phtml'
            ),
            'template_path_stack' => array(
                __DIR__ . '/../view',
            ),
        ),
        'doctrine' => array(
            'driver' => array(
                'application_entities' => array(
                    'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                    'cache' => 'array',
                    'paths' => array(__DIR__ . '/../src/User/Entity')
                ),
                'orm_default' => array(
                    'drivers' => array(
                        'User\Entity' => 'application_entities'
                    )
                )
            )
        ),
    )
);