<?php

return array(
    'router' => array(
        'routes' => array(
            'admin' => array(
                'child_routes' => array(

                    'user' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/user',
                            'defaults' => array(
                                'controller' => 'User\Controller\UserController',
                                'action' => 'index',
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'edit' => array(
                                'type' => 'Segment',
                                'options' => array(
                                    'route' => '/edit[/:id]',
                                    'constraints' => array(
                                        'id' => '[0-9]*',
                                    ),
                                    'defaults' => array(
                                        'controller' => 'User\Controller\UserController',
                                        'action' => 'edit',
                                    ),
                                ),
                            ),
                            'delete' => array(
                                'type' => 'Segment',
                                'options' => array(
                                    'route' => '/delete[/:id]',
                                    'constraints' => array(
                                        'id' => '[0-9]*',
                                    ),
                                    'defaults' => array(
                                        'controller' => 'User\Controller\UserController',
                                        'action' => 'delete',
                                    ),
                                ),
                            ),
                            'xhr-user-table' => array(
                                'type' => 'Segment',
                                'options' => array(
                                    'route' => '/xhr-table',
                                    'constraints' => array(),
                                    'defaults' => array(
                                        'controller' => 'User\Controller\UserController',
                                        'action' => 'xhrUserTable',
                                    ),
                                ),
                            ),
                        ),
                    ),

                ),
            ),
        ),
    ),
);