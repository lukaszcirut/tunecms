<?php

namespace User\Controller;

use System\Controller\AbstractDashboardController;
use User\Entity\Users;
use User\Form\UserForm;
use User\View\UsersTrait;
use Zend\View\Model\ViewModel;

class UserController extends AbstractDashboardController
{
    use UsersTrait;

    public function indexAction()
    {

        $box = $this->getUsersTable();

        $createForm = $this->prepareUserCreateForm();


        return new ViewModel(array(
            'userLists' => $box->render(),
            'createForm' => $createForm->render()
        ));
    }

    public function xhrUserTableAction()
    {
        $table = $this->getXhrUsersTable();
        return $this->getResponse()->setContent($table->render());
    }


}
