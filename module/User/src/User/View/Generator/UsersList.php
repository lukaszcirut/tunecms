<?php

namespace User\View\Generator;

use ZfTable\AbstractTable;

class UsersList extends AbstractTable
{
    protected $config = array(
        'name' => 'Users List',
        'showPagination' => true,
        'showQuickSearch' => false,
        'showItemPerPage' => true,
        'itemCountPerPage' => 20,
        'showColumnFilters' => true,
        'valuesOfItemPerPage' => array(1, 5, 10, 20, 50),
        'rowAction' => '/asdasd/asdasd',
        'tableAlias' => 'us'
    );

    //Definition of headers
    protected $headers = array(
        'id' => array('title' => 'Id', 'width' => '50') ,
        'email' => array('title' => 'Email', 'editable' => true),
        'login' => array('title' => 'Active' , 'width' => 100 ),
        'isOnline' => array('title' => 'Online' , 'width' => 100, 'sortable' => false ),
    );

    public function init()
    {
    }

    protected function initFilters($query)
    {

    }
}