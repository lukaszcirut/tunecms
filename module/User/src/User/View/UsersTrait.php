<?php

namespace User\View;

use System\View\Box\LteBoxGenerator;
use User\Entity\Users;
use User\Form\UserForm;
use User\View\Generator\UsersList;
use User\View\Widget\UserCreate;
use Zend\ServiceManager\ServiceLocatorInterface;

trait UsersTrait
{

    public function getUsersTable($options = array())
    {
        $widget = new \User\View\Widget\UsersList($this->getServiceLocator());
        $widget->setName('List of all registered users');
        $widget->setWidth(6);
        $box = new LteBoxGenerator(null, $widget);
        $box->setFooterContent('If you want to <strong>add some user</strong>, use beyond form');

        return $box;
    }

    public function getXhrUsersTable()
    {
        $users = $this->getEntityManager()->getRepository('User\Entity\Users');
        $table = new UsersList();
        $table->setSource($users->createQueryBuilder('us'))
            ->setParamAdapter($this->getRequest()->getPost());

        return $table;
    }

    public function prepareUserCreateForm()
    {
        $form = new UserForm('user');
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $user = new Users();
            $form->setObject($user);
            $form->setData($request->getPost());

            if ($form->isValid()) {
                /** @var Users $newUser */
                $newUser = $form->getData();
                $this->getEntityManager()->persist($newUser);
                $this->getEntityManager()->flush();

                // Redirect to list of albums
                return $this->redirect()->toRoute('admin/user');
            }
        }

        $widget = new UserCreate($this->getServiceLocator());
        $widget->setName('Create new user');
        $widget->setWidth(6);
        $widget->setForm($form);
        $box = new LteBoxGenerator(null, $widget);

        return $box;
    }

}