<?php

namespace User\View\Widget;


use System\View\Widget\AbstractWidget;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Model\ViewModel;

class UserCreate extends AbstractWidget
{
    /** @var  ServiceLocatorInterface */
    protected $serviceLocator;
    protected $form;

    /**
     * UsersList constructor.
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        $this->setWidth('12');
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function render()
    {
        $viewRender = $this->getServiceLocator()->get('ViewRenderer');
        $viewModel = new ViewModel(array('form' => $this->getForm()));
        $viewModel->setTemplate('user/widget/create');
        $html = $viewRender->render($viewModel);

        return $html;
    }

    /**
     * @return mixed
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param mixed $form
     */
    public function setForm($form)
    {
        $this->form = $form;
    }

}