<?php

namespace User\View\Widget;


use System\View\Widget\AbstractWidget;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Model\ViewModel;

class UsersList extends AbstractWidget
{
    /** @var  ServiceLocatorInterface */
    protected $serviceLocator;

    /**
     * UsersList constructor.
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        $this->setWidth('12');
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }



    public function render()
    {
        $viewRender = $this->getServiceLocator()->get('ViewRenderer');
        $viewModel = new ViewModel();
        $viewModel->setTemplate('user/widget/list');
        $html = $viewRender->render($viewModel);

        return $html;
    }

}